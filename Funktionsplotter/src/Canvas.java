import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {

    public Canvas() {
        setBackground(Color.BLACK);
        setForeground(Color.RED);
        setPreferredSize(new Dimension(500,500));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawLine(250,0,250,500);
        g.drawLine(245,230,255,230);
        g.drawString("+1",255,230);
        g.drawLine(245,270,255,270);
        g.drawString("-1",255,270);
        g.drawLine(0,250,500,250);
        g.drawLine(230,245,230,255);
        g.drawString("-1",230,245);
        g.drawLine(270,245,270,255);
        g.drawString("+1",270,245);
    }
}
