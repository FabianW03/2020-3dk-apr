import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BarMovingKeyAdapter extends KeyAdapter {

    private final PingPongBar bar;

    public BarMovingKeyAdapter(PingPongBar bar) {
        this.bar = bar;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_LEFT:
                bar.moveLeft();
                break;
            case KeyEvent.VK_RIGHT:
                bar.moveRight();
                break;
        }
    }
}
