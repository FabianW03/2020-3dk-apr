import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EnglishPersonTest {

    @Test
    void greet() {
        EnglishPerson englishPerson = new EnglishPerson("Tim");
        assertEquals("Hello Tim!", englishPerson.greet());

    }

    @Test
    void greetWithNickname() {
        EnglishPerson englishPerson = new EnglishPerson("Johann");
        assertEquals("Hello Hansi!", englishPerson.greet());
    }
}
