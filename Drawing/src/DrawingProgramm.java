import javax.swing.*;

public class DrawingProgramm extends JFrame{

    public DrawingProgramm(String title){
        super(title);
        setSize(500,750);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        Canvas canvas = new Canvas();
        add(canvas);

        setVisible(true);
    }
}
