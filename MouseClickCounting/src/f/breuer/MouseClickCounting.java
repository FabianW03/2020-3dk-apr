package f.breuer;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseClickCounting extends MouseAdapter {

    private int clicks = 0;

    @Override
    public void mouseClicked(MouseEvent e) {
        clicks++;
    }

    public int getClicks() {
        return clicks;
    }
}
