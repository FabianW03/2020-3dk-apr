import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class GaesteregestrierungGui extends JFrame {

    public GaesteregestrierungGui(){
        super("COVID-19 Gastregestrierung");
        setSize(550,300);
        setLayout(new FlowLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setPreferredSize(new Dimension(550,25));
        panel.setBorder(new LineBorder(Color.GREEN,1));

        JLabel label = new JLabel("Gästeregistrierung");
        label.setBounds(175,0,200,25);
        label.setFont(new Font("Segoe UI",Font.BOLD,18));
        panel.add(label);
        add(panel);

        GaesteregistrierungFields grgf = new GaesteregistrierungFields();
        add(grgf);

        setVisible(true);
        //pack();
    }
}
