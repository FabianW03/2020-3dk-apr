import java.sql.*;

public class UserDataAccessObject {

    private final String username;
    private final String password;

    private final String connectionUrl;

    public UserDataAccessObject(String host, String port, String databaseName, String username, String password) {
        this.connectionUrl = "jdbc:mariadb://"+ host +":"+ port +"/"+databaseName;
        this.username = username;
        this.password = password;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver");
        }
    }

    private Connection connect(){
        try {
            return DriverManager.getConnection(this.connectionUrl,this.username,this.password);
        } catch (SQLException throwables){
            throw new RuntimeException();
        }
    }

    private void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.out.println("Closed connection");
        }
    }

    public boolean login(String username, String password){
        Connection connection = connect();
        String sql = "select * from user where username='"+username+"' and password='"+password+"'";
        Statement login = null;
        try {
            login = connection.createStatement();
            ResultSet resultSet = login.executeQuery(sql);
            if (resultSet.next()){
                return true;
            }
            return false;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not run login query",throwables);
        } finally {
            close(connection);
        }
    }

    public boolean secureLogin(String userName, String password){
        Connection connection = connect();
        String sql = "select * from user where username = ? and password = ?";
        try {
            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            prepareStatement.setString(1,userName);
            prepareStatement.setString(2,password);

            ResultSet resultSet = prepareStatement.executeQuery();
            if (resultSet.next()){
                return true;
            }
            return false;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not run preparedStatement");
        } finally {
            close(connection);
        }
    }
}
