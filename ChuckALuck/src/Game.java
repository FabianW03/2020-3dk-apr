// wäre schöner, wenn diese Klasse Methoden zur Verfügung stellt, zBsp. würfeln, Punkte berechnen
public class Game {

    private int score;

    public Game(SetNumber setNumber, ThrowDice throwDice, Bankaccount bankaccount) {
        score = 0;

        bankaccount.getBtnAddStake().addActionListener(e -> {
            if (bankaccount.getLblScore() == 0){
                score+=5;
                bankaccount.setLblScore(score);

                throwDice.setBtnDice(1);
                bankaccount.setBtnAddStake(0);
            }
        });

        throwDice.getBtnDice().addActionListener(e -> {
            if (bankaccount.getLblScore() > 0){

                throwDice.throwDice();
                int[] numbers = new int[]{throwDice.getLblDice1(),throwDice.getLblDice2(),throwDice.getLblDice3()};
                int pastscore = score;

                if (setNumber.getTxtSetNumber() <= 6 && setNumber.getTxtSetNumber() >= 1 ){
                    if (setNumber.getTxtSetNumber() == numbers[0] || setNumber.getTxtSetNumber() == numbers[1] || setNumber.getTxtSetNumber() == numbers[2]) {
                        for (int num:numbers) {
                            if (setNumber.getTxtSetNumber() == num){
                                score++;
                                bankaccount.setLblScore(score);
                            }
                        }
                    } else if (pastscore == score){
                        score--;
                        bankaccount.setLblScore(score);
                    }
                } else {
                    System.out.println("!!!Ungültige Eingabe!!!");
                }
            } else {
                throwDice.setBtnDice(0);
                bankaccount.setBtnAddStake(1);
            }
        });

    }
}