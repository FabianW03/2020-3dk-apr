import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class SetNumber extends JPanel {

    private JTextField txtSetNumber;

    public SetNumber() {
        setLayout(new FlowLayout(FlowLayout.CENTER,10,20));
        setBorder(new EmptyBorder(6,10,10,10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.MAGENTA);

        JLabel lblSetNumber = new JLabel("Zahl setzen");
        lblSetNumber.setHorizontalAlignment(SwingConstants.CENTER);
        lblSetNumber.setPreferredSize(new Dimension(120,30));
        lblSetNumber.setOpaque(true);
        lblSetNumber.setBackground(Color.WHITE);
        add(lblSetNumber);

        txtSetNumber = new JTextField();
        txtSetNumber.setHorizontalAlignment(SwingConstants.CENTER);
        txtSetNumber.setPreferredSize(new Dimension(30,30));
        txtSetNumber.setOpaque(true);
        txtSetNumber.setBackground(Color.WHITE);
        add(txtSetNumber);

    }

    public int getTxtSetNumber() {
        return Integer.parseInt(txtSetNumber.getText());
    }
}

